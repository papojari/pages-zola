<div align="center">

<h1>Template for the <a href="https://www.getzola.org/">Zola static site generator</a></h1>

Generated using [Zola](https://www.getzola.org/)

<p>
	<a href="https://papojari.codeberg.page"><img src="https://forthebadge.com/images/badges/check-it-out.svg">
</p>

[Build output repository](https://codeberg.org/papojari/pages)

## [© License](LICENSE.md)

</div>