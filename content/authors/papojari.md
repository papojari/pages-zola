+++
title = "papojari"
description = "Owner of this website"
date = 2021-12-19 16:47:00
draft = false
+++

I design websites, develop games and write packages and my system configuration in Nix. Occasionally I'll create something in Blender, a 3d software. I really dig Linux stuff. My desktop operating system is [NixOS](https://nixos.org/). As you can read, I heavily use open source software. Yes I say *open source* because saying *free* (as in freedom) leaves out *copyleft* software. Commiting to a git repository is very satisfying btw.
