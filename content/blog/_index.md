+++
title = "Blog"
description = "Blog"
sort_by = "date"
paginate_by = 10
insert_anchor_links = "left"
template = "blog/section.html"
page_template = "blog/page.html"
+++