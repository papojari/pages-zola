+++
title = "NixOS 3d showcase"
date = 2021-06-05

[extra]
software = "Blender"
render_engine = "Cycles"
pbr_materials = ""

[taxonomies]
authors = ["papojari"]
+++

I created this piece as a background for my desktop. It tries to showcase what [NixOS](https://nixos.org) is about.
