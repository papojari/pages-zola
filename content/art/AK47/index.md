+++
title = "AK47"
date = 2021-01-03

[extra]
software = "Blender"
render_engine = "Cycles"
pbr_materials = "Concrete, metal and wood from ambientCG"

[taxonomies]
authors = ["papojari"]
+++