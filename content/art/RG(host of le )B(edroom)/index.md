+++
title = "RG(host of le )B(edroom)"
date = 2021-08-08

[extra]
software = "Blender"
render_engine = "Cycles"
pbr_materials = "ambientCG"

[taxonomies]
authors = ["papojari"]
+++

Without the PBR materials from ambientCG this would not look half as good. They're all licensed under CC0. With this piece I combined a bunch of stuff like modelling a PC and a whole bunch of other things with softbody and cloth simulations to make a nice room. I improved my compositing skills quite a bit too. All the work took several days though the time was probably greatly reduced by my *new* hardware like a better processor and graphics card.
