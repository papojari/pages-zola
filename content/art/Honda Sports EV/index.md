+++
title = "Honda Sports EV"
date = 2021-02-09

[extra]
software = "Blender"
render_engine = "Cycles"
pbr_materials = "Walls and floor from ambientCG"

[taxonomies]
authors = ["papojari"]
+++

I modelled this car off the Honda Sports EV Concept from 2018.
