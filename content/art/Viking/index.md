+++
title = "Viking"
date = 2021-02-09

[extra]
software = "Blender"
render_engine = "Cycles"
pbr_materials = ""

[taxonomies]
authors = ["papojari"]
+++

This is a Fanart of mine from the game *[Scrap mechanic](https://www.scrapmechanic.com/)*.
